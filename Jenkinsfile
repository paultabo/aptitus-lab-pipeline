#!/usr/bin/env groovy

def fnSteps = evaluate readTrusted("deploy/steps.groovy")

pipeline {
  agent any
  environment {
    DEVELOPMENT_ENV = 'dev'
    STAGING_ENV = 'pre'
    PRODUCTION_ENV = 'prod'
  }
  parameters {
    booleanParam(name: 'DEVELOPMENT', defaultValue: true, description: "Ejecutar Development")
    booleanParam(name: 'STAGING', defaultValue: true, description: "Ejecutar Staging")
    booleanParam(name: 'PRODUCTION', defaultValue: true, description: "Ejecutar Production")
    choice(
      name: 'ENVIRONMENT',
      choices:"dev\npre\nprod",
      description: '''DEV: Desplegar en dev
PRE: Desplegar en pre
PROD: Desplegar en prod'''
    )
    choice(
      name: 'EXECUTE',
      choices:"DEPLOY\nROLLBACK\nREGISTRY",
      description: '''DEPLOY: Se realiza deploy del servicio
ROLLBACK: Rollback de la última migración
REGISTRY: Requiere construir o no Registry en ECR'''
    )
  }
  stages {
    stage('Checkout') {
      steps {
        checkout scm
      }
    }

    /* #################### Development #################### */

    stage('Development Config') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.DEVELOPMENT }
      }
      steps {
        script {
          config = fnSteps.configs(DEVELOPMENT_ENV)
        }
      }
    }
    stage('Development') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.DEVELOPMENT }
      }
      parallel {
        stage('Deploy BACK') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployBack(config) } }
        }
        stage('Deploy FRONT') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployFront(config) } }
        }
        stage('Rollback') {
          when { expression { return params.EXECUTE == 'ROLLBACK' }}
          steps { script { fnSteps.rollback(config) } }
        }
      }
    }

    stage('Tests E2E') {
      steps {
        echo 'make test-project'
      }
    }

    /* #################### Staging #################### */

    stage ('Staging Config') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.STAGING }
      }
      steps {
        input "Continue deployment to Staging?"
        script {
          config = fnSteps.configs(DEVELOPMENT_ENV)
        }
      }
    }

    stage('Staging') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.STAGING }
      }
      parallel {
        stage('Deploy BACK') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployBack(config) } }
        }
        stage('Deploy FRONT') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployFront(config) } }
        }
        stage('Rollback') {
          when { expression { return params.EXECUTE == 'ROLLBACK' }}
          steps { script { fnSteps.rollback(config) } }
        }
      }
    }

    /* #################### Production #################### */

    stage ('Production Config') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.PRODUCTION }
      }
      steps {
        input "Continue deployment to Production?"
        script {
          config = fnSteps.configs(DEVELOPMENT_ENV)
        }
      }
    }

    stage('Production') {
      when {
        allOf { environment name: 'CHANGE_ID', value: ''}
        expression { return params.PRODUCTION }
      }
      parallel {
        stage('Deploy BACK') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployBack(config) } }
        }
        stage('Deploy FRONT') {
          when { expression { return params.EXECUTE == 'DEPLOY' }}
          steps { script { fnSteps.deployFront(config) } }
        }
        stage('Rollback') {
          when { expression { return params.EXECUTE == 'ROLLBACK' }}
          steps { script { fnSteps.rollback(config) } }
        }
      }
    }
  }
}
