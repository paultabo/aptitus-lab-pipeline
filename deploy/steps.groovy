def deployBack(def config) {
    echo 'make install'
    echo 'make build-latest'
    echo 'make migrate'
    echo 'make publish'
    echo 'make update-service'
}

def deployFront(def config) {
    echo 'make install'
    echo 'make build-latest'
}

def rollback(def config) {
    echo 'make install'
    echo 'make rollback'
}

def registry(def config) {
    echo 'make create-registry'
}

def configs(def enviroment) {
  def config = [
    "ENV=${enviroment}",
    "INFRA_BUCKET=infraestructura.${enviroment}"
  ]

  return config
}

return this
